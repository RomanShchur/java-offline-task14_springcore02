package task14_SpringCore02.model;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.stereotype.Component;

@Configuration
public class Configuration02 {
  @Configuration
  @ComponentScan(basePackages = "task14_SpringCore02.model.beans2",
    includeFilters = @ComponentScan.Filter(type = FilterType.REGEX, pattern =
      "*Flower.*"))
  public static class Filter1 {}

  @Configuration
  @ComponentScan(basePackages = "task14_SpringCore02.model.beans3",
    excludeFilters = @ComponentScan.Filter(type = FilterType.REGEX, pattern =
      "BeanE.*"))
  public static class Filter2 {}
}
