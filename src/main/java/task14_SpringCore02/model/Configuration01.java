package task14_SpringCore02.model;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;

@Configuration
@ComponentScan(basePackages = "task14_SpringCore02.model.beans1")
public class Configuration01 {
}
