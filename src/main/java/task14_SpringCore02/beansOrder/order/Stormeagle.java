package task14_SpringCore02.beansOrder.order;

import org.springframework.context.annotation.Profile;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Profile("comp")
@Component
@Order(3)
public class Stormeagle implements Craft {
  public String getCraftModel() {
    return "Stormeagle";
  }
}
