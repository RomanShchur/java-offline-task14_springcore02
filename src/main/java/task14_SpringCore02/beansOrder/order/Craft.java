package task14_SpringCore02.beansOrder.order;

public interface Craft {
  public String getCraftModel();
}
