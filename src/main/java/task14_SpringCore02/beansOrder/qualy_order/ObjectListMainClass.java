package task14_SpringCore02.beansOrder.qualy_order;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import task14_SpringCore02.beansOrder.qualy_order.Craft;

import java.util.List;


@Component
public class ObjectListMainClass {
  @Autowired
  private List<Craft> crafts;
  public void printNames() {
    for(Craft craft : crafts) {
      System.out.println(craft.getCraftModel());
    }
  }
}
