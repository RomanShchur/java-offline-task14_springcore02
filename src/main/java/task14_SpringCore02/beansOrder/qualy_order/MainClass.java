package task14_SpringCore02.beansOrder.qualy_order;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class MainClass {
  public static void main(String[] args) {
    ApplicationContext context =
      new AnnotationConfigApplicationContext(Config.class);
    context.getBean(ObjectListMainClass.class).printNames();
  }
}
