package task14_SpringCore02.beansOrder.qualy_order;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Qualifier("stormeagle")
public class Stormeagle implements Craft {
  public String getCraftModel() {
    return "Stormeagle";
  }
}
