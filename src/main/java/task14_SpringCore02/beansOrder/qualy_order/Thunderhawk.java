package task14_SpringCore02.beansOrder.qualy_order;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

@Component
@Primary
public class Thunderhawk implements Craft {
  public String getCraftModel() {
    return "Thunderhawk";
  }
}
