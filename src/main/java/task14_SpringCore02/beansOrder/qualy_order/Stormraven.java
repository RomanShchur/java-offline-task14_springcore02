package task14_SpringCore02.beansOrder.qualy_order;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Qualifier("stormraven")
public class Stormraven implements Craft {
  public String getCraftModel() {
    return "Stormraven";
  }
}
