package task14_SpringCore02.dependencyInjection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
public class Stormeagle {
  @Autowired private String model;
  @Autowired private String type;
  @Autowired private Integer crew;
  @Autowired private String mainWeapon;
  @Autowired private String secondaryWeapon;
  @Autowired private String powerplants;
  @Autowired private Integer speed;
  @Autowired private Integer range;
  @Autowired private Double weight;
  @Autowired private Double length;
  @Autowired private Double wingspawn;
  @Autowired private Double height;
  @Autowired private String origin;

  public String getModel() {
    return model;
  }
  public String getType() {
    return type;
  }
  public Integer getCrew() {
    return crew;
  }
  public String getMainWeapon() {
    return mainWeapon;
  }
  public String getSecondaryWeapon() {
    return secondaryWeapon;
  }
  public String getPowerplants() {
    return powerplants;
  }
  public Integer getSpeed() {
    return speed;
  }
  public Integer getRange() {
    return range;
  }
  public Double getWeight() {
    return weight;
  }
  public Double getLength() {
    return length;
  }
  public Double getWingspawn() {
    return wingspawn;
  }
  public Double getHeight() {
    return height;
  }
  public String getOrigin() {
    return origin;
  }

  @Autowired
  public void setModel(String model) {
    this.model = model;
  }
  @Autowired
  public void setType(String type) {
    this.type = type;
  }
  @Autowired
  public void setCrew(Integer crew) {
    this.crew = crew;
  }
  @Autowired
  public void setMainWeapon(String mainWeapon) {
    this.mainWeapon = mainWeapon;
  }
  @Autowired
  public void setSecondaryWeapon(String secondaryWeapon) {
    this.secondaryWeapon = secondaryWeapon;
  }
  @Autowired
  public void setPowerplants(String powerplants) {
    this.powerplants = powerplants;
  }
  @Autowired
  public void setSpeed(Integer speed) {
    this.speed = speed;
  }
  @Autowired
  public void setRange(Integer range) {
    this.range = range;
  }
  @Autowired
  public void setWeight(Double weight) {
    this.weight = weight;
  }
  @Autowired
  public void setLength(Double length) {
    this.length = length;
  }
  @Autowired
  public void setWingspawn(Double wingspawn) {
    this.wingspawn = wingspawn;
  }
  @Autowired
  public void setHeight(Double height) {
    this.height = height;
  }
  @Autowired
  public void setOrigin(String origin) {
    this.origin = origin;
  }
}
