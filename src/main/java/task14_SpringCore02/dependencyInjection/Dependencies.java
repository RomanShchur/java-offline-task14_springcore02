package task14_SpringCore02.dependencyInjection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan
public class Dependencies {
  @Bean
  Tunderhawk tunderhawk() {
    return new Tunderhawk(
      "Thunderhawk Gunship",
      "Orbital Dropship",
      4,
      "Dorsal mounted Battle Cannon",
      "4x twin-linked Heavy Bolters",
      "3 x RX-92-00 Combination Rocket/Afterburning Turbofans",
      2000,
      28000,
      121.0,
      26.6,
      26.65,
      9.8,
      "Mars"
    );
  }
  @Bean
  Stormeagle stormeagle() {
    Stormeagle stormeagle01 = new Stormeagle();
    stormeagle01.setModel("Storm Eagle");
    stormeagle01.setType("Orbital Dropship");
    stormeagle01.setCrew(1);
    stormeagle01.setMainWeapon("Vengeance launcher");
    stormeagle01.setSecondaryWeapon("Twin-linked Heavy Bolters");
    stormeagle01.setPowerplants("2x combination rocket/afterburning turbofans");
    stormeagle01.setSpeed(2000);
    stormeagle01.setRange(30000);
    stormeagle01.setWeight(68.0);
    stormeagle01.setLength(18.0);
    stormeagle01.setWingspawn(11.8);
    stormeagle01.setHeight(7.2);
    stormeagle01.setOrigin("Unknown");
    return stormeagle01;
  }
  @Bean
  Valkirie valkirie() {
    Valkirie valkirie01 = new Valkirie();
    valkirie01.model = "Valkyrie";
    valkirie01.type = "Airborne Assault Carrie";
    valkirie01.crew = 4;
    valkirie01.mainWeapon = "Hull mounted Multi-laser";
    valkirie01.secondaryWeapon = "2x Hellstrike Missiles";
    valkirie01.powerplants = "2x F75-MV Afterburning Vector-Turbojets";
    valkirie01.speed = 1100;
    valkirie01.range = 2000;
    valkirie01.weight = 31.0;
    valkirie01.length = 18.5;
    valkirie01.wingspawn = 16.9;
    valkirie01.height = 4.8;
    valkirie01.origin = "Mars";
    return valkirie01;
  }
}
