package task14_SpringCore02.dependencyInjection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class Valkirie {
  @Autowired
  public String model;
  @Autowired
  public String type;
  @Autowired
  public Integer crew;
  @Autowired
  public String mainWeapon;
  @Autowired
  public String secondaryWeapon;
  @Autowired
  public String powerplants;
  @Autowired
  public Integer speed;
  @Autowired
  public Integer range;
  @Autowired
  public Double weight;
  @Autowired
  public Double length;
  @Autowired
  public Double wingspawn;
  @Autowired
  public Double height;
  @Autowired
  public String origin;

  public String getModel() {
    return model;
  }
  public String getType() {
    return type;
  }
  public Integer getCrew() {
    return crew;
  }
  public String getMainWeapon() {
    return mainWeapon;
  }
  public String getSecondaryWeapon() {
    return secondaryWeapon;
  }
  public String getPowerplants() {
    return powerplants;
  }
  public Integer getSpeed() {
    return speed;
  }
  public Integer getRange() {
    return range;
  }
  public Double getWeight() {
    return weight;
  }
  public Double getLength() {
    return length;
  }
  public Double getWingspawn() {
    return wingspawn;
  }
  public Double getHeight() {
    return height;
  }
  public String getOrigin() {
    return origin;
  }
}
