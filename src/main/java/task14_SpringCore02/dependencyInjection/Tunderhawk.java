package task14_SpringCore02.dependencyInjection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("singleton")
public class Tunderhawk {
  private String model;
  private String type;
  private Integer crew;
  private String mainWeapon;
  private String secondaryWeapon;
  private String powerplants;
  private Integer speed;
  private Integer range;
  private Double weight;
  private Double length;
  private Double wingspawn;
  private Double height;
  private String origin;

  @Autowired
  public Tunderhawk(String model, String type, Integer crew, String mainWeapon,
    String secondaryWeapon, String powerplants, Integer speed, Integer range,
    Double weight, Double length, Double wingspawn, Double height,
    String origin) {
    this.model = model;
    this.type = type;
    this.crew = crew;
    this.mainWeapon = mainWeapon;
    this.secondaryWeapon = secondaryWeapon;
    this.powerplants = powerplants;
    this.speed = speed;
    this.range = range;
    this.weight = weight;
    this.length = length;
    this.wingspawn = wingspawn;
    this.height = height;
    this.origin = origin;
  }

  public String getModel() {
    return model;
  }
  public String getType() {
    return type;
  }
  public Integer getCrew() {
    return crew;
  }
  public String getMainWeapon() {
    return mainWeapon;
  }
  public String getSecondaryWeapon() {
    return secondaryWeapon;
  }
  public String getPowerplants() {
    return powerplants;
  }
  public Integer getSpeed() {
    return speed;
  }
  public Integer getRange() {
    return range;
  }
  public Double getWeight() {
    return weight;
  }
  public Double getLength() {
    return length;
  }
  public Double getWingspawn() {
    return wingspawn;
  }
  public Double getHeight() {
    return height;
  }
  public String getOrigin() {
    return origin;
  }
}
