package task14_SpringCore02.dependencyInjection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Component;

public class MainInjectionClass {
  @Autowired
  public static void main(String[] args) {
    ApplicationContext context = new AnnotationConfigApplicationContext(Dependencies.class);
    System.out.println(context.getBean(Tunderhawk.class));
    System.out.println(context.getBean(Tunderhawk.class));
    System.out.println(context.getBean(Stormeagle.class));
    System.out.println(context.getBean(Stormeagle.class));
    System.out.println(context.getBean(Valkirie.class));
    System.out.println(context.getBean(Valkirie.class));
  }
}
